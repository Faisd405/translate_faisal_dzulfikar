<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','TranslateController@index');
Route::get('/show', 'TranslateController@show');
Route::get('/showid', 'TranslateController@showid');
// Route::get('/show/{inggris}', 'TranslateController@show');
