<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
require_once '../vendor/autoload.php';
use Statickidz\GoogleTranslate;

class TranslateController extends Controller
{
    public function index(){
        return view('welcome');
    }
    public function show(Request $request){
        $request->validate([
            'inggris' => 'required',
        ]);
        $source = "id";
        $target = "en";
        $text = "";
        $text = $request["inggris"];
        $trans = new GoogleTranslate();
        $result = $trans->translate($source, $target, $text);
    	return view('show',['translate' => $result]);
    }
    public function showid(Request $request){
        $request->validate([
            'indonesia' => 'required',
        ]);
        $source = "en";
        $target = "id";
        $text = "";
        $text = $request["indonesia"];
        $trans = new GoogleTranslate();
        $result = $trans->translate($source, $target, $text);
    	return view('show',['translate' => $result]);
    }
}
